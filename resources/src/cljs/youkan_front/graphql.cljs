(ns youkan-front.graphql
  (:require
   [re-frame.core :as rf]
   [venia.core :as v]))

(defn login [email password]
  (let [gql (v/graphql-query {:venia/operation {:operation/type :mutation
                                                :operation/name "login"}
                              :venia/variables [{:variable/name "email"
                                                 :variable/type :String!}
                                                {:variable/name "password"
                                                 :variable/type :String!}]
                              :venia/queries   [[:signUp {:email    email
                                                          :password password}
                                                 [:allocation :name]]]})]))
    ;(socket/send-ws gql)))

(defn signup [username email password]
  (let [gql (v/graphql-query {:venia/operation {:operation/type :mutation
                                                :operation/name "signup"}
                              :venia/variables [{:variable/name "username"
                                                 :variable/type :String!}
                                                {:variable/name "email"
                                                 :variable/type :String!}
                                                {:variable/name "password"
                                                 :variable/type :String!}]
                              :venia/queries   [[:signUp {:username username
                                                          :email    email
                                                          :password password}
                                                 [:allocation :name]]]})]))
    ;(socket/send-ws gql)))

