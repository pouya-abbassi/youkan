(ns youkan-front.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
  :signup-modal
  (fn [db _]
    (get-in db [:modal :signup])))

(rf/reg-sub
  :login-modal
  (fn [db _]
    (get-in db [:modal :login])))

(rf/reg-sub
  :login-help-email
  (fn [db _]
    (get-in db [:login-help :email])))

(rf/reg-sub
  :login-help-password
  (fn [db _]
    (get-in db [:login-help :password])))

(rf/reg-sub
  :login-help-submit
  (fn [db _]
    (get-in db [:login-help :submit])))

(rf/reg-sub
  :signup-help-username
  (fn [db _]
    (get-in db [:signup-help :username])))

(rf/reg-sub
  :signup-help-email
  (fn [db _]
    (get-in db [:signup-help :email])))

(rf/reg-sub
  :signup-help-password
  (fn [db _]
    (get-in db [:signup-help :password])))

(rf/reg-sub
  :signup-help-repeat
  (fn [db _]
    (get-in db [:signup-help :repeat])))

(rf/reg-sub
  :signup-help-submit
  (fn [db _]
    (get-in db [:signup-help :submit])))

(rf/reg-sub
  :burger
  (fn [db _]
    (get-in db [:burger])))

