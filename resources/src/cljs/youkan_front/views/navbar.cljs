(ns youkan-front.views.navbar
  (:require
    [re-frame.core :as rf]
    [youkan-front.views.login :as login]))

(defn home []
      [:nav {:class "navbar" :role "navigation" :aria-label "main navigation" :style {:background "#FFFFFFAA"}}
        [:div.navbar-brand
          [:a {:class "navbar-item" :href "/"}
            [:img {:src "/img/logo.svg"}]]
          [:a {:class (str "navbar-burger burger " @(rf/subscribe [:burger])) :role "button" :aria-label "menu" :aria-expanded "false" :on-click #(rf/dispatch [:burger])}
           [:span {:aria-hidden "true"}]
           [:span {:aria-hidden "true"}]
           [:span {:aria-hidden "true"}]]]
        [:div {:class (str "navbar-menu " @(rf/subscribe [:burger]))}
         [:div {:class "navbar-start"}
          [:a {:class "navbar-item" :href "/"}
           [:span.icon [:i {:class "icon fa fa-home"}]]
           [:span "home"]]
          [:a {:class "navbar-item" :href "/about"}
           [:span.icon [:i.fa.fa-file-alt]]
           [:span "About"]]
          [:a {:class "navbar-item" :on-click #(rf/dispatch [:send-ping])}
           [:span.icon [:i.fa.fa-ethernet]]
           [:span "Ping server"]]]
         [:div.navbar-end
          [:div.navbar-item
           [:div.buttons
            [:a.button.is-primary {:on-click #(rf/dispatch [:show-signup])}
             [:strong "Sign up"]]
            [:a.button.is-light {:on-click #(rf/dispatch [:show-login])}
             [:span "Log in"]]]]]]
        [login/login]
        [login/signup]])

