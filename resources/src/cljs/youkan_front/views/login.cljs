(ns youkan-front.views.login
  (:require
    [re-frame.core :as rf]))

(defn login []
  [:div {:class (str "modal " @(rf/subscribe [:login-modal]))}
   [:div.modal-background {:on-click #(rf/dispatch [:hide-modals])}]
   [:button {:class "modal-close is-large" :aria-label "close" :on-click #(rf/dispatch [:hide-modals])}]
   [:div.modal-content
    [:div.box
     [:h3.subtitle.has-text-centered "Login"]
     [:div.field
      [:label.label "Email:"]
      [:div {:class "control has-icons-left has-icons-right"}
       [:input {:class (str "input " @(rf/subscribe [:login-help-email])) :on-change #(rf/dispatch [:login-email (-> % .-target .-value)]) :type "text" :placeholder "Email"}]
       [:span.icon.is-small.is-left
        [:i.fa.fa-at]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:login-help-email]))} "eg. name@something.com"]]
     [:div.field
      [:label.label "Password:"]
      [:div {:class "control has-icons-left has-icons-right"}
       [:input {:class (str "input " @(rf/subscribe [:login-help-password])) :on-change #(rf/dispatch [:login-password (-> % .-target .-value)]) :type "password" :placeholder "Password"}]
       [:span.icon.is-small.is-left
        [:i.fa.fa-key]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:login-help-password]))} "Must contian uppercase, lowercase and numbers, at least 8 character."]]
     [:div.field.is-grouped.is-grouped-centered
      [:p.control
       [:a {:class "button is-primary" :on-click #(rf/dispatch [:submit-login])} "Submit"]]
      [:p.control
       [:a.button.is-light {:on-click #(rf/dispatch [:hide-modals])} "Cancel"]]]
     [:p {:class (str "has-text-centered help " @(rf/subscribe [:login-help-submit]))} "Please fill the above form, then push the Submit button."]
     [:hr]
     [:p.has-text-centered "Don't have account? "
      [:button {:class "button is-link is-small" :on-click #(rf/dispatch [:show-signup])} "Sign up"]]]]])

(defn signup []
  [:div {:class (str "modal " @(rf/subscribe [:signup-modal]))}
   [:div.modal-background {:on-click #(rf/dispatch [:hide-modals])}]
   [:button {:class "modal-close is-large" :aria-label "close" :on-click #(rf/dispatch [:hide-modals])}]
   [:div.modal-content
    [:div.box
     [:h3.subtitle.has-text-centered "Sign up"]
     [:div.field
      [:label.label "Username:"]
      [:div {:class "control has-icons-left"}
       [:input {:class (str "input " @(rf/subscribe [:signup-help-username])) :on-change #(rf/dispatch [:signup-username (-> % .-target .-value)]) :type "text" :placeholder "Username"}]
       [:span.icon.is-small.is-left
        [:i.fa.fa-user]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:signup-help-username]))} "Your username. it's not for login purpose, just to identify and mention."
       [:br] "May contain lowercase characters, numbers, dot and underscore. May not start or end with dot or underscore."]]
     [:div.field
      [:label.label "Email:"]
      [:div {:class "control has-icons-left has-icons-right"}
       [:input {:class (str "input " @(rf/subscribe [:signup-help-email])) :on-change #(rf/dispatch [:signup-email (-> % .-target .-value)]) :type "text" :placeholder "Email"}]
       [:span.icon.is-small.is-left
        [:i.fa.fa-at]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:signup-help-email]))} "eg. name@something.com"]]
     [:div.field
      [:label.label "Password:"]
      [:div {:class "control has-icons-left has-icons-right"}
       [:input {:class (str "input " @(rf/subscribe [:signup-help-password])) :on-change #(rf/dispatch [:signup-password (-> % .-target .-value)]) :type "password" :placeholder "Password"}]
       [:span.icon.is-small.is-left
        [:i.fa.fa-key]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:signup-help-password]))} "Must contian uppercase, lowercase and numbers, at least 8 character."]]
     [:div.field
      [:label.label "Repeat:"]
      [:div {:class "control has-icons-left has-icons-right"}
       [:input {:class (str "input " @(rf/subscribe [:signup-help-repeat])) :on-change #(rf/dispatch [:signup-repeat (-> % .-target .-value)]) :type "password" :placeholder "Repeat"}]
       [:span.icon.is-.is-left
        [:i.fa.fa-key]
        [:i]]]
      [:p {:class (str "help " @(rf/subscribe [:signup-help-repeat]))} "Must be the same as Password."]]
     [:div.field
      [:div.control
       [:label.checkbox
        [:input {:type "checkbox"}] " I agree to the " [:a {:href "/terms"} "terms and conditions"] "."]]]
     [:div.field.is-grouped.is-grouped-centered
      [:p.control
       [:a {:class "button is-primary" :on-click #(rf/dispatch [:submit-signup])} "Submit"]]
      [:p.control
       [:a.button.is-light {:on-click #(rf/dispatch [:hide-modals])} "Cancel"]]]
     [:p {:class (str "has-text-centered help " @(rf/subscribe [:signup-help-submit]))} "Please fill the above form, then push the Submit button."]
     [:hr]
     [:p.has-text-centered "Already have account? "
      [:button {:class "button is-link is-small" :on-click #(rf/dispatch [:show-login])} "Log in"]]]]])

