(ns youkan-front.input-validation
  (:require
   [clojure.spec.alpha :as s]))

(def email-regex #"^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$") ; No capital letter allowed, must contain `@` and `.` and "top-level domain" must be at least 2 character.
(def password-regex #"^(?=.*\d)(?=.*[a-zA-Z])(?!.*(\s)).{8,32}$") ; Must contain lowercase and uppercase and number, at least 8 character.
(def username_regex #"^(?!(\.))(?!(\_))([a-z0-9_\.]{2,15})[a-z0-9]$") ; No capital letter allowed, can contain `_` and `.`, can't start with number or `_` or `.`, can't end with `_` or `.`.

(s/def ::email #(re-matches email-regex %))
(s/def ::password #(re-matches password-regex %))
(s/def ::username #(re-matches username_regex %))

(defn email [email]
  (s/valid? ::email email))

(defn password [password]
  (s/valid? ::password password))

(defn username [username]
  (s/valid? ::username username))
