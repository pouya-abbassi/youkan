(ns youkan-front.views
  (:require
   [re-frame.core :as re-frame]
   [youkan-front.subs :as subs]
   [youkan-front.views.navbar :as navbar]))

(defn main-panel []
  [:div
    [:div#home
     [navbar/home]
     [:section {:class "hero is-fullheight-with-navbar"}
       [:div {:class "hero-body"}
         [:div.container
           [:div.columns {:style {:background "#FFFFFFAA"}}
            [:div.column.is-5
             [:figure.image.is-512x512
              [:img {:src "https://placeimg.com/512/512/tech"}]]]
            [:div.column.is-7.has-text-centered.is-vcentered
             [:h1.title "You Kan!"]
             [:h3.subtitle "We will help you."]
             [:div.content.has-text-left
              [:h2 "Kanban"]
              [:p.subtitle "Kanban can help you stay focused in your progress."]
              [:h2 "Teamwork"]
              [:p.subtitle "Work, talk and collaborate with your team. Real-time!"]
              [:h2 "Time"]
              [:p.subtitle "Manage your time schedule the modern way."]]]]]]]]])

