(ns youkan.graphql
  (:require [com.walmartlabs.lacinia.schema :as schema]
            [com.walmartlabs.lacinia :as lac]))

; Simple graphql scheme. For testing.
(def hello-schema (schema/compile
                   {:queries {:hello
                              {:type 'String
                               :resolve (constantly "world")}}}))

(defn simple [query]
  (first (:data (lac/execute hello-schema query nil nil))))

