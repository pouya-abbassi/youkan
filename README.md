# YouKan

> Under development. Not ready just yet.

Kanban software to help you aim better and monitor your progress along the way.

## Getting Started

0. Clone the repo and enter the directory: `git clone git@gitlab.com:pouya-abbassi/youkan.git && cd youkan`
1. Start the back-end repl: `lein repl`
2. To start the server, run this in repl: `(def dev-serv (run-dev))` (learn more about [repl development with pedestal](http://pedestal.io/guides/developing-at-the-repl))
3. Go to [localhost:8080](http://localhost:8080/).

Back-end ([pedestal](http://pedestal.io/)) is ready. You can edit and evaluate functions.

0. Open up another terminal emulator and enter front-end directory: `cd resources`
1. Start ClojureScript repl: `lein figwheel dev`

Front-end ([figwheel](https://figwheel.org/), [re-agent](https://reagent-project.github.io/), [re-frame](https://github.com/Day8/re-frame)) is working. You can edit and save files, figwheel will automatically reload the page.

0. Open up another terminal emulator for Sass: `sass resources/src/sass/site.sass resources/public/css/site.css --watch` for "compressed" version add `--style=compressed`.

Now sass will automatically compile stylesheet.

## License
[![Creative Commons License](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

